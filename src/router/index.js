import Vue from 'vue'
import Router from 'vue-router'


Vue.use(Router)
export default new  Router({
  routes: [{
    // 首页
    path: '/',
    component: function (resolve) {
      require(['../page/index'], resolve)
    }
  },{
    // 商品详情
    path: '/detail',
    component: function (resolve) {
    require(['../page/detail'], resolve)
    }
  },{
    // 搜索
    path: '/search',
    component: function(resolve) {
      require(['../page/search'], resolve)
    }
  },{
    // 购物车
    path: '/shop_cart',
    component: function(resolve) {
      require(['../page/shop_cart'], resolve)
    }
  },{
    // 结算
    path: '/settlement',
    component: function(resolve) {
      require(['../page/settlement'], resolve)
    }
  },{
    // 我的订单
    path: '/order',
    component: function(resolve) {
      require(['../page/order'], resolve)
    }
  },{
    // 预约
    path: '/make_appointment',
    component: function(resolve) {
      require(['../page/make_appointment'], resolve)
    }
  },{
    // 预约成功
    path: '/reservations',
    component: function(resolve) {
      require(['../page/reservations'], resolve)
    }
  },{
    // 我的
    path: '/me',
    component: function(resolve) {
      require(['../page/me'], resolve)
    }
  },{
    // 余额支付
    path: '/balance_payment',
    component: function(resolve) {
      require(['../page/balance_payment'], resolve)
    }
  },{
    // 次卡
    path: '/frequency',
    component: function(resolve) {
      require(['../page/frequency'], resolve)
    }
  },{
    // 优惠券
    path: '/coupon',
    component: function(resolve) {
      require(['../page/coupon'], resolve)
    }
  },{
    // 个人设置
    path: '/set',
    component: function(resolve) {
      require(['../page/set'], resolve)
    }
  },{
    // 地址
    path: '/address',
    component: function(resolve) {
      require(['../page/address'], resolve)
    }
  },{
    // 添加地址
    path: '/add_address',
    component: function(resolve) {
      require(['../page/add-address'], resolve)
    }
  }]  
})

