// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import FastClick from 'fastclick'
// import VueRouter from 'vue-router'
import App from './App'
import router from './router/index'
import Vuex from 'vuex'
import $ from 'jquery'


Vue.use(Vuex)
// Plugin调用vux
import { DatetimePlugin } from 'vux'
Vue.use(DatetimePlugin)
// Vue.use(VueRouter)

// const routes = router;

// const router = new VueRouter({
//   routes
// })

FastClick.attach(document.body)

Vue.config.productionTip = false
// vuex-load
const store = new Vuex.Store({}) // 这里你可能已经有其他 module

store.registerModule('vux', { // 名字自己定义
  state: {
    isLoading: false,
    count:0
  },
  mutations: {
    increment (state) {
      state.count++
      alert(state.count)
    },
    updateLoadingStatus (state, payload) {
      state.isLoading = payload.isLoading
    }
  }
})
router.beforeEach(function (to, from, next) {
  store.commit('updateLoadingStatus', {isLoading: true});
  // store.commit('increment');
  next()
})

router.afterEach(function (to) {
  store.commit('updateLoadingStatus', {isLoading: false})
})
/* eslint-disable no-new */
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app-box')


/* 下面开始是全局函数 */
// 订餐的数据
var order_food={
  ordered_food:[{
    uid: '',//商品的uid
    num: '',//商品数量
    option: {},//选项
  }],//已经点的食物
  /**
   * @description 更新已点商品的属性和数量
   * @param {number} uid - 商品的uid
   */
  update:function(){
    
  },//更新已点食物的信息，比如数量，口味
  detele:function(){

  },//删除已点食物
  get:function(){
    return ordered_food
  },//获取订购的商品信息
  add:function(){
    
  },//增加新商品到订单
}
window.order_food=order_food;

